# -*- coding: utf-8 -*-
{
    'name': "IMG-Product",

    'summary': """
        Module for managing the image - product
        """,

    'description': """
        * Module for managing the image - product *
    """,

    'author': "qzlab",
    'website': "http://www.qzlab.com",

    'category': 'extra-addons',
    'version': '13.0.0.0',

    # any module necessary for this one to work correctly
    'depends': ['base_setup'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/product.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
