from odoo import models,fields,api
from odoo.exceptions import UserError
import PIL.Image as Image
from PIL import ImageDraw
from PIL import ImageFont 
import io
import base64

FONT = ImageFont.truetype("/home/maratmustafin/Desktop/odoo/extra/photo_changer/models/DejaVuSans.ttf", 18)

DEPTH_COORD = (
        (332,52),
        (280,206),
        (230,337)
)

DIAMETR_COORD = (
        (332,68),
        (280,222),
        (230,353)
)

DIAMETR_SIGN = '⌀'


class ImageProduct(models.Model):
    _name = 'image.product'
    _description = 'product description'


    name = fields.Char(string='Название насоса')
    img1 = fields.Image(string='Изображение насоса')
    img2 = fields.Binary(string='Изображение',attachment=True)
    depth1 = fields.Integer(string='Кондуктор (метр)',default = 0)
    diametr1 = fields.Integer(string='Кондуктор (диаметр)',default = 0)
    # x_depth1 = fields.Char(string='x1',default=35)
    # y_depth1 = fields.Char(string='y1',default=170)
    depth2 = fields.Integer(string='Направление (метр)',default=0)
    diametr2 = fields.Integer(string='Направление (диаметр)',default = 0)
    # x_depth2 = fields.Char(string='x2',default=250)
    # y_depth2 = fields.Char(string='y2',default=150)
    depth3 = fields.Integer(string='Техническая колонна (метр)',default = 0)
    diametr3 = fields.Integer(string='Техническая колонна (диаметр)',default = 0)
    # x_depth3 = fields.Char(string='x3',default=250)
    # y_depth3 = fields.Char(string='y3',default=200)


    ####################### CREATE ########################

    @api.model
    def create(self,values):
        result = super(ImageProduct,self).create(values)
        return self._create_image(result)

    # private method for create
    def _create_image(self,instance):
        """method for creating image with watermark"""
        if instance.img1 is False:
            raise UserError('Картинка не найдена')
        if instance.depth1 is False and instance.diametr1 is False:
            raise UserError('Добавьте данные в поле <Кондуктор>')
        if instance.depth2 is False and instance.diametr2 is False:
            raise UserError('Добавьте данные в поле <Направление>')
        if instance.depth3 is False and instance.diametr3 is False:
            raise UserError('Добавьте данные в поле <Техническая колонна>')
    

        b = base64.decodestring(instance.img1)
        img = Image.open(io.BytesIO(b))

        drawing = ImageDraw.Draw(img)
        black = (0, 0, 0)

        self._drawing_instance_depth(drawing,instance,black)
        self._drawing_instance_diametr(drawing,instance,black)
        decode_string = self._decode_image(img)

        instance.img2 = base64.b64encode(decode_string).decode("utf-8")
        return instance     
    
    
    def _drawing_instance_depth(self,drawing,instance,color):
        drawing.text((DEPTH_COORD[0][0],DEPTH_COORD[0][1]), str(instance.depth1) + ' м.', fill=color, font=FONT)
        drawing.text((DEPTH_COORD[1][0],DEPTH_COORD[1][1]), str(instance.depth2) + ' м.', fill=color, font=FONT)
        drawing.text((DEPTH_COORD[2][0],DEPTH_COORD[2][1]), str(instance.depth3) + ' м.', fill=color, font=FONT)

    def _drawing_instance_diametr(self,drawing,instance,color):
        drawing.text((DIAMETR_COORD[0][0],DIAMETR_COORD[0][1]), DIAMETR_SIGN+str(instance.depth1), fill=color, font=FONT)
        drawing.text((DIAMETR_COORD[1][0],DIAMETR_COORD[1][1]), DIAMETR_SIGN+str(instance.depth2), fill=color, font=FONT)
        drawing.text((DIAMETR_COORD[2][0],DIAMETR_COORD[2][1]), DIAMETR_SIGN+str(instance.depth3), fill=color, font=FONT)

    ################# UPDATE ######################

    def write(self,values):
        updated_values = self._update_image(values)
        result = super(ImageProduct, self).write(updated_values)
        return result


    # private method for update
    def _update_image(self,values):
        depths,diametrs = self._check_fields(values)

        b = base64.decodestring(self.img1)
        img = Image.open(io.BytesIO(b))
        drawing = ImageDraw.Draw(img)
        black = (0, 0, 0)

        self._drawing_depth(drawing,depths,black)
        self._drawing_diametr(drawing,diametrs,black)
        decode_string = self._decode_image(img)
        values.update({'img2':base64.b64encode(decode_string).decode("utf-8")})
        return values


    def _check_fields(self,values):
        depth1 = values.get('depth1') if values.get('depth1') != None else self.depth1
        depth2 = values.get('depth2') if values.get('depth2') != None else self.depth2
        depth3 = values.get('depth3') if values.get('depth3') != None else self.depth3
        diametr1 = values.get('diametr1') if values.get('diametr1') != None else self.diametr1
        diametr2 = values.get('diametr2') if values.get('diametr2') != None else self.diametr2
        diametr3 = values.get('diametr3') if values.get('diametr3') != None else self.diametr3
        depths = [depth1,depth2,depth3]
        diametrs = [diametr1,diametr2,diametr3]
        return (depths,diametrs)

    def _drawing_depth(self,drawing,depth,color):
        drawing.text((DEPTH_COORD[0][0],DEPTH_COORD[0][1]), str(depth[0]) +' м.', fill=color, font=FONT)
        drawing.text((DEPTH_COORD[1][0],DEPTH_COORD[1][1]), str(depth[1]) +' м.', fill=color, font=FONT)
        drawing.text((DEPTH_COORD[2][0],DEPTH_COORD[2][1]), str(depth[2])+' м.', fill=color, font=FONT)

    def _drawing_diametr(self,drawing,diametr,color):
        drawing.text((DIAMETR_COORD[0][0],DIAMETR_COORD[0][1]), DIAMETR_SIGN+str(diametr[0]), fill=color, font=FONT)
        drawing.text((DIAMETR_COORD[1][0],DIAMETR_COORD[1][1]), DIAMETR_SIGN+str(diametr[1]), fill=color, font=FONT)
        drawing.text((DIAMETR_COORD[2][0],DIAMETR_COORD[2][1]), DIAMETR_SIGN+str(diametr[2]), fill=color, font=FONT)


    
    # general method
    def _decode_image(self,img):
        img_byte_array = io.BytesIO()
        img.save(img_byte_array,format=img.format)
        img_byte_array = img_byte_array.getvalue()
        return img_byte_array 
