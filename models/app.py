#!/usr/bin/env python
# coding: utf-8# In[29]:from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import Image


image = Image.open("/home/maratmustafin/Desktop/odoo/extra/photo_changer/models/Скважина.png")
# image.show()

watermark_image = image.copy()
  
draw = ImageDraw.Draw(watermark_image)
font = ImageFont.truetype("/home/maratmustafin/Desktop/odoo/extra/photo_changer/models/DejaVuSans.ttf", 18)
black = (0,0,0)
draw.text((332,52),'⌀'+'200', fill=black, font=font)
draw.text((332,68),'200' + ' м.', fill=black, font=font)
draw.text((280,206),  '⌀'+'2000', fill=black, font=font)
draw.text((280,222), '200' + ' м.', fill=black, font=font)
draw.text((230,337), '⌀'+'500', fill=black, font=font)
draw.text((230,353),'200' + ' м.', fill=black, font=font)
watermark_image.rotate(30)
watermark_image.show()
